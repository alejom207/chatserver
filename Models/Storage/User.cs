using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace ChatServer.Models.Storage
{
    public class User
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
        [NotMapped]
        [JsonProperty("password-confirmation")]
        public string PasswordConfirmation { get; set; }
        [JsonProperty("nickname")]
        public string Nickname { get; set; }
    }
}