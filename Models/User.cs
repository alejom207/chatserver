using Newtonsoft.Json;

namespace ChatServer.Models
{
    public class User
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("nickname")]
        public string Nickname { get; set; }
    }
}