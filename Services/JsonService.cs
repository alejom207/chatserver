using System;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ChatServer.Services
{
    public class JsonService : IJsonService
    {
        public async Task<T> GetDeserializedResponse<T>(string json)
        {
            try
            {
                return await Task.Run(() => JsonConvert.DeserializeObject<T>(json)).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
                return default(T);
            }
        }

        public async Task<string> GetSerializedResponse(object objectToConvert)
        {
            try
            {
                return await Task.Run(() => JsonConvert.SerializeObject(objectToConvert)).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
                return string.Empty;
            }
        }
    }
}